﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Masiv.OnlineBetting.Infraestructure.Repository
{
    public abstract class BaseRepository<T>
    {
        protected readonly string _connectionString;
        protected SqlCommand cmd;
        protected SqlDataReader dr;

        public BaseRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("defaultConnection");
        }

        protected SqlCommand PrepareStoreProcedure(string spName, SqlConnection connection = null)
        {
            if (connection == null)
                connection = new SqlConnection(_connectionString);
            cmd = new SqlCommand(spName, connection);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 7200;
            return cmd;
        }

        protected async Task ExecuteNonQueryStoreProcedureAsync(SqlCommand cmd)
        {
            try
            {
                await cmd.Connection.OpenAsync();
                await cmd.ExecuteNonQueryAsync();
            }
            catch (SqlException ex)
            {
                throw new BaseException("Error al realizar la operación.", ex);
            }
            finally
            {
                if (cmd.Connection.State == ConnectionState.Open)
                {
                    cmd.Connection.Close();
                    cmd.Dispose();
                }
            }
        }

        protected async Task<List<A>> ExecuteQueryStoreProcedureAsync<A>(SqlCommand cmd, Func<SqlDataReader, A> fnFromDataReader)
        {
            List<A> list = new List<A>();
            try
            {
                await cmd.Connection.OpenAsync();
                dr = await cmd.ExecuteReaderAsync();
                while (dr.Read())
                {
                    list.Add(fnFromDataReader(dr));
                }
            }
            catch (SqlException ex)
            {
                throw new BaseException("Error al realizar la consulta.", ex);
            }
            finally
            {
                if (dr != null)
                {
                    dr.Close();
                }
                if (cmd.Connection.State == ConnectionState.Open)
                {
                    cmd.Connection.Close();
                    cmd.Dispose();
                }
            }
            return list;
        }

        public virtual T FromDataReader(SqlDataReader reader) { throw new NotImplementedException(); }
    }
}
