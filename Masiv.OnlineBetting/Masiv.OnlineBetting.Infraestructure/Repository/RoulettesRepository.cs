﻿using Masiv.OnlineBetting.Core.Entities;
using Masiv.OnlineBetting.Core.Enums;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Masiv.OnlineBetting.Infraestructure.Repository
{
    public class RoulettesRepository : BaseRepository<Roulette>
    {
        public RoulettesRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public async Task<Roulette> GetAsync(int id)
        {
            var cmd = PrepareStoreProcedure("API.Rolulette_Get");
            cmd.Parameters.Add("@pROULETTE_ID", SqlDbType.Int).Value = id;
            var ent = (await ExecuteQueryStoreProcedureAsync(cmd, FromDataReader)).FirstOrDefault();
            return ent;
        }

        public async Task<List<Roulette>> GetAsync()
        {
            var cmd = PrepareStoreProcedure("API.Rolulette_Get");
            var list = await ExecuteQueryStoreProcedureAsync(cmd, FromDataReader);
            return list;
        }

        public async Task<int> CreateAsync()
        {
            var cmd = PrepareStoreProcedure("API.Rolulette_Create");
            cmd.Parameters.Add("@pROULETTE_ID", SqlDbType.Int);
            cmd.Parameters["@pROULETTE_ID"].Direction = ParameterDirection.Output;
            await ExecuteNonQueryStoreProcedureAsync(cmd);
            int id = Convert.ToInt32(cmd.Parameters["@pROULETTE_ID"].Value);
            return id;
        }

        public async Task OpenAsync(int id)
        {
            var cmd = PrepareStoreProcedure("API.Rolulette_Open");
            cmd.Parameters.Add("@pROULETTE_ID", SqlDbType.Int).Value = id;
            await ExecuteNonQueryStoreProcedureAsync(cmd);
        }

        public async Task CloseAsync(int id)
        {
            int winnerNumber = Roulette.GetWinnerNumber;
            var winnercolor = Roulette.GetWinnerColor(winnerNumber);
            var cmd = PrepareStoreProcedure("API.Rolulette_Close");
            cmd.Parameters.Add("@pROULETTE_ID", SqlDbType.Int).Value = id;
            cmd.Parameters.Add("@pWINNER_NUMBER", SqlDbType.Int).Value = winnerNumber;
            cmd.Parameters.Add("@pWINNER_COLOR", SqlDbType.Int).Value = (int)winnercolor;
            await ExecuteNonQueryStoreProcedureAsync(cmd);
        }

        public static new Roulette FromDataReader(SqlDataReader dr)
        {
            var entity = new Roulette
            {
                ID = Convert.ToInt32(dr["ROULETTE_ID"]),
                Status = (RouletteStatus)Convert.ToInt32(dr["ROULETTE_STATUS"])
            };
            entity.WinnerNumber = dr["ROULETTE_WINNER_NUMBER"] == DBNull.Value ? entity.WinnerNumber : Convert.ToInt32(dr["ROULETTE_WINNER_NUMBER"]);
            return entity;
        }
    }
}
