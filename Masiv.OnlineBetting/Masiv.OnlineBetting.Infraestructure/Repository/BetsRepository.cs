﻿using Masiv.OnlineBetting.Core.Entities;
using Masiv.OnlineBetting.Core.Enums;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Masiv.OnlineBetting.Infraestructure.Repository
{
    public class BetsRepository : BaseRepository<Bet>
    {
        public BetsRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public async Task<List<Bet>> GetAsync(int RouletteID)
        {
            var cmd = PrepareStoreProcedure("API.Bet_Get");
            cmd.Parameters.Add("@pROULETTE_ID", SqlDbType.Int).Value = RouletteID;
            var list = await ExecuteQueryStoreProcedureAsync(cmd, FromDataReader);
            return list;
        }

        public async Task<int> CreateAsync(Bet bet)
        {
            var cmd = PrepareStoreProcedure("API.Bet_Create");
            cmd.Parameters.Add("@pBET_ID", SqlDbType.Int);
            cmd.Parameters["@pBET_ID"].Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@pROULETTE_ID", SqlDbType.Int).Value = bet.RouletteID;
            cmd.Parameters.Add("@pUSER_ID", SqlDbType.Int).Value = bet.UserID;
            cmd.Parameters.Add("@pBET_NUMBER", SqlDbType.Int).Value = bet.Number;
            cmd.Parameters.Add("@pBET_COLOR", SqlDbType.Int).Value = bet.Color;
            cmd.Parameters.Add("@pBET_AMOUNT", SqlDbType.Decimal).Value = bet.Amount;
            await ExecuteNonQueryStoreProcedureAsync(cmd);
            int id = Convert.ToInt32(cmd.Parameters["@pBET_ID"].Value);
            return id;
        }

        public static new Bet FromDataReader(SqlDataReader dr)
        {
            var entity = new Bet
            {
                UserID = Convert.ToInt32(dr["USER_ID"]),
                RouletteID = Convert.ToInt32(dr["ROULETTE_ID"]),
                Amount = Convert.ToDecimal(dr["BET_AMOUNT"]),
                AmountEarned = Convert.ToDecimal(dr["BET_AMOUNT_EARNED"])
            };
            entity.Number = dr["BET_NUMBER"] == DBNull.Value ? entity.Number : Convert.ToInt32(dr["BET_NUMBER"]);
            entity.Color = dr["BET_COLOR"] == DBNull.Value ? entity.Color : (BetColor)Convert.ToInt32(dr["BET_COLOR"]);
            return entity;
        }
    }
}
