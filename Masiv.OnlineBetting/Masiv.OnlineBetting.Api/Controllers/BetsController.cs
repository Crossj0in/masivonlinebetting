﻿using AutoMapper;
using Masiv.OnlineBetting.Api.DTOs;
using Masiv.OnlineBetting.Core.Entities;
using Masiv.OnlineBetting.Core.Enums;
using Masiv.OnlineBetting.Infraestructure.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Threading.Tasks;

namespace Masiv.OnlineBetting.Api.Controllers
{
    [Route("api/v1/bets")]
    [ApiController]
    public class BetsController : ControllerBase
    {
        private readonly RoulettesRepository _rouleteRepository;
        private readonly BetsRepository _betRepository;
        private readonly IMapper maper;
        public BetsController(BetsRepository betRepository, RoulettesRepository rouleteRepository, IMapper maper)
        {
            _betRepository = betRepository;
            _rouleteRepository = rouleteRepository;
            this.maper = maper;
        }

        [HttpPost]
        public async Task<ActionResult> Create(BetDTO dto)
        {
            string userID = Request.Headers["UserID"];
            if (string.IsNullOrEmpty(userID) || !userID.All(char.IsNumber))
                throw new AuthenticationException();

            Roulette roulette = await _rouleteRepository.GetAsync(dto.RouletteID);
            if (roulette.Status != RouletteStatus.Open)
                return BadRequest();

            var bet = maper.Map<Bet>(dto);
            bet.UserID = Convert.ToInt32(userID);

            await _betRepository.CreateAsync(bet);
            return Ok();
        }
    }
}
