﻿using AutoMapper;
using Masiv.OnlineBetting.Api.DTOs;
using Masiv.OnlineBetting.Core.Entities;
using Masiv.OnlineBetting.Infraestructure.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Masiv.OnlineBetting.Api.Controllers
{
    [Route("api/v1/roulettes")]
    [ApiController]
    public class RoulettesController : ControllerBase
    {
        private readonly RoulettesRepository _rouletesRepository;
        private readonly BetsRepository _betsRepository;
        private readonly IMapper maper;
        public RoulettesController(RoulettesRepository rouleteRepository, BetsRepository betRepository, IMapper maper)
        {
            _rouletesRepository = rouleteRepository;
            _betsRepository = betRepository;
            this.maper = maper;
        }

        [HttpGet]
        public async Task<ActionResult<List<Roulette>>> Get()
        {
            var list = await _rouletesRepository.GetAsync();
            return Ok(list);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Roulette>> Get(int id)
        {
            var ent = await _rouletesRepository.GetAsync(id);
            return Ok(ent);
        }

        [HttpPost]
        public async Task<ActionResult> Create()
        {
            int newRouletteID = await _rouletesRepository.CreateAsync();
            return Ok(newRouletteID);
        }

        [HttpPut("open/{id}")]
        public async Task<ActionResult> Open(int id)
        {
            await _rouletesRepository.OpenAsync(id);
            return Ok();
        }

        [HttpPut("close/{id}")]
        public async Task<ActionResult> Close(int id)
        {
            await _rouletesRepository.CloseAsync(id);
            var roulette = await _rouletesRepository.GetAsync(id);
            var result = maper.Map<RouletteDTO>(roulette);
            result.Bets = await _betsRepository.GetAsync(RouletteID: id);
            return Ok(result);
        }
    }
}
