﻿using Masiv.OnlineBetting.Core;
using Masiv.OnlineBetting.Core.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Masiv.OnlineBetting.Api.DTOs
{
    public class BetDTO : IValidatableObject
    {
        [Required]
        public int RouletteID { get; set; }
        [Range(Constants.MinNumber, Constants.MaxNumber)]
        public int? Number { get; set; } = null;
        public BetColor Color { get; set; } = BetColor.NoColor;
        [Required]
        [Range(Constants.MinAmount, Constants.MaxAmount)]
        public decimal Amount { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (Number == null && Color == BetColor.NoColor)
                yield return new ValidationResult("Color requerido");
        }
    }
}
