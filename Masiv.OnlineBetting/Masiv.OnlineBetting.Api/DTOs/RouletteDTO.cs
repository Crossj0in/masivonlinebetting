﻿using Masiv.OnlineBetting.Core.Entities;
using Masiv.OnlineBetting.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Masiv.OnlineBetting.Api.DTOs
{
    public class RouletteDTO
    {
        public int ID { get; set; }
        public RouletteStatus Status { get; set; } = RouletteStatus.Created;
        public int? WinnerNumber { get; set; }
        public List<Bet> Bets { get; set; } = new List<Bet>();
    }
}
