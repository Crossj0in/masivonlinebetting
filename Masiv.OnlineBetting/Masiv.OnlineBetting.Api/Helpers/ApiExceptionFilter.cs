﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Authentication;
using System.Threading.Tasks;

namespace Masiv.OnlineBetting.Api.Helpers
{
    public class ApiExceptionFilter : ExceptionFilterAttribute
    {
        public override Task OnExceptionAsync(ExceptionContext context)
        {
            string mensaje = "Error inesperado.";
            context.HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

            if (context.Exception is NotImplementedException)
            {
                mensaje = "No implementado.";
                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.NotImplemented;
            }
            else if (context.Exception is AuthenticationException)
            {
                mensaje = "No tiene permiso para acceder a este recurso.";
                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Forbidden;
            }

            var error = new ApiError
            {
                Message = mensaje,
                Detail = context.Exception.InnerException != null ? context.Exception.InnerException.Message : ""
            };
            context.Result = new JsonResult(error);

            base.OnException(context);
            return Task.CompletedTask;
        }
    }
}
