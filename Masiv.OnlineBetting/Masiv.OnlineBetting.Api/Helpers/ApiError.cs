﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Masiv.OnlineBetting.Api.Helpers
{
    public class ApiError
    {
        public string Message { get; set; }
        public bool IsError { get; set; } = true;
        public string Detail { get; set; }
    }
}
