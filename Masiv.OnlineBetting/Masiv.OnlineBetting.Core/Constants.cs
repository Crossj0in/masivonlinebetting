﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Masiv.OnlineBetting.Core
{
    public static class Constants
    {
        public const int MinNumber = 0;
        public const int MaxNumber = 36;

        public const int MinAmount = 1;
        public const int MaxAmount = 10000;
    }
}
