﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Masiv.OnlineBetting.Core.Enums
{
    public enum BetColor
    {
        NoColor,
        Black = 100,
        Red = 101
    }
}
