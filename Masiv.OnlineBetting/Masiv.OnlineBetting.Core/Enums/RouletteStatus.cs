﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Masiv.OnlineBetting.Core.Enums
{
    public enum RouletteStatus
    {
        Created = 1,
        Open = 2,
        Closed = 0
    }
}
