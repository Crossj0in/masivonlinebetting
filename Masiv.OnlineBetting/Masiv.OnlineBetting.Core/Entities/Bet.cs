﻿using Masiv.OnlineBetting.Core.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Masiv.OnlineBetting.Core.Entities
{
    public class Bet : IValidatableObject
    {
        [Required]
        public int UserID { get; set; }
        [Required]
        public int RouletteID { get; set; }
        [Range(Constants.MinNumber, Constants.MaxNumber)]
        public int? Number { get; set; }
        public BetColor Color { get; set; } = BetColor.NoColor;
        [Required]
        [Range(Constants.MinAmount, Constants.MaxAmount)]
        public decimal Amount { get; set; }
        public decimal AmountEarned { get; set; } = 0;

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (Number == null && Color == BetColor.NoColor)
                yield return new ValidationResult("Color requerido");
        }
    }
}
