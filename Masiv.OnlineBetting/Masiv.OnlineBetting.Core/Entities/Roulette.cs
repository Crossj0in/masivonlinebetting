﻿using Masiv.OnlineBetting.Core.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Masiv.OnlineBetting.Core.Entities
{
    public class Roulette
    {
        public int ID { get; set; }
        public RouletteStatus Status { get; set; } = RouletteStatus.Created;
        public int? WinnerNumber { get; set; }

        public static int GetWinnerNumber
        {
            get
            {
                Random random = new Random();
                int randomNumber = random.Next(Constants.MinNumber, Constants.MaxNumber);
                return randomNumber;
            }
        }

        public static BetColor GetWinnerColor(int number)
        {
            return (number % 2 == 0 ? BetColor.Red : BetColor.Black);
        }
    }
}
