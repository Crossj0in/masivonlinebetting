# Masiv Online Betting

Clean code test

## Tools
- Visual Studio 2019 Community
- SQL Server 2019 Developer Edition
- Postman

## Installation

1. Run DB scripts in order (located in DB)
2. Open the solution, run [clean] and [recompile]
3. You can also run the following commands

```bash
dotnet clear
dotnet restore
dotnet build
```

## Use Cases
1. Endpoint to create roulette
```bash
[POST] /api/v1/roulettes
```
2. Endpoint to open roulette
```bash
[PUT] /api/v1/roulettes/open/{id}
```
3. Endpoint to betting (send entity bet and UserID Header)
```bash
[POST] /api/v1/bets/
```
4. Endpoint to close roulette
```bash
[PUT] /api/v1/roulettes/close/{id}
```
5. Endpoint to list roulettes
```bash
[GET] /api/v1/roulettes
```