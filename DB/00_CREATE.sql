USE master;
GO

DECLARE @kill VARCHAR(8000) = '';  
SELECT @kill = @kill + 'kill ' + CONVERT(VARCHAR(5), session_id) + ';'
FROM sys.dm_exec_sessions
WHERE database_id  = DB_ID('db_roulette')
EXEC(@kill);
GO

If(db_id(N'db_roulette') IS NOT NULL)
	DROP DATABASE db_roulette;
GO

CREATE DATABASE db_roulette;
GO

USE db_roulette;
GO

CREATE SCHEMA API;
GO

GO
IF EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'API.ROULETTE') AND TYPE = N'U')
    DROP TABLE API.ROULETTE;
GO
CREATE TABLE API.ROULETTE (
	ROULETTE_ID				INT IDENTITY(1,1) PRIMARY KEY,
	ROULETTE_STATUS			INT DEFAULT 1,

	ROULETTE_WINNER_NUMBER	INT,
	ROULETTE_AMOUNT_EARNED	DECIMAL(18,2) DEFAULT 0,

	ROULETTE_CREATE_DATE	DATETIME DEFAULT CURRENT_TIMESTAMP,
	ROULETTE_MODIFI_DATE	DATETIME NULL,
)
GO
PRINT('API.ROULETTE		=> CREATE');
GO

GO
IF EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'API.BET') AND TYPE = N'U')
    DROP TABLE API.BET;
GO
CREATE TABLE API.BET (
	BET_ID			INT IDENTITY(1,1) PRIMARY KEY,
	USER_ID			INT NULL,
	ROULETTE_ID		INT NULL,
	BET_NUMBER		INT NULL CHECK(BET_NUMBER >= 0 AND BET_NUMBER <= 36),
	BET_COLOR		INT NULL,
	BET_AMOUNT		DECIMAL(7,2) NOT NULL CHECK(BET_AMOUNT >= 1 AND BET_AMOUNT <= 10000),
	BET_AMOUNT_EARNED	DECIMAL(18,2) DEFAULT 0,

	BET_AMONUT_CREATE_DATE	DATETIME DEFAULT CURRENT_TIMESTAMP,

	UNIQUE (ROULETTE_ID, USER_ID)
)
GO
PRINT('API.BET		=> CREATE');
GO